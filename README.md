# SSG

Minimal **S**tatic **S**ite **G**enerator written in perl. This software was
inspired in [sw](https://github.com/jroimartin/sw), a suckless webframework.

## Installation

There's no installation method for now. The *ssh* and *styles.css* should be on the same
directory, and the result directory will be there as well.

## Usage

ssg [switches] input-directory

where switches can be:

* -v  print version and exits
* -h  print this help and exits
* -o  set the input directory (by default site.static/)

By default the output directory is *./site.static/* (on the same directory as
this program

## Generation + upload

You can automate all the process creating the following script.

    ./ssg -o mysite input-dir
    rsync -avz mysite/ your.site:/path/to/www

## Author

Matias Linares <matias@deprecated.org>
